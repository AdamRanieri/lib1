import {client} from '../src/connection'

// client is the main object we will use to interact with our database
console.log(process.env.DBPASSWORD)

test("Should create a connection ", async ()=>{
    const result = await client.query('select * from book'); // we need await becuase we
    // are fetching data stored on a database in the cloud
    
});

afterAll(async()=>{
    client.end()// should close our connection once all tests is over
})

